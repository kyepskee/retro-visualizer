package main

import (
	"math"
	"math/rand"
	"math/cmplx"
	"fmt"
)

type Grid interface {
	Perlin()
	GetGlGrid(...[]float64) []float32
	Flatten(int)
}

type grid struct {
	width, height int
	values        [][]float32
	origin        vector3

	dx, dy, dz vector3
	color      vector3
	glGrid     []float32
}

func NewGrid(w, h int, o, dx, dy, dz, c vector3) Grid {
	v := make([][]float32, w)
	for i := 0; i < w; i++ {
		v[i] = make([]float32, h)
		for j := 0; j < h; j++ {
			v[i][j] = 1
		}
	}
	g := &grid{
		w, h,
		v,
		o,
		dx, dy, dz,
		c,
		[]float32{},
	}
	g.glGrid = g.newGlGrid()
	return g
}

func (g *grid) Perlin() {
	nx, ny := 5, 5
	noiseGrid := make([][]float32, nx*ny)

	for i := range noiseGrid {
		// makes a complex number to represent a unit vec in a random direction
		angle := 2*math.Pi*rand.Float64()
		vec := cmplx.Rect(1, angle)
		noiseGrid[i] = []float32{float32(real(vec)), float32(imag(vec))}
	}

	// linear interpolation of a and b with weight w
	interp := func(a, b, w float32) float32 {
		return w*(a-b) + b
	}

	// gets the dot product of the vector from the
	// point at (cx, cy) with the vector from it to (x, y)
	dot := func(cx, cy int, x, y float32) float32 {
		dx, dy := x-float32(cx), y-float32(cy)
		idx := cy*nx + cx
		if cx >= nx || cy >= ny {
			return 0
		}
		return dx*noiseGrid[idx][0] + dy*noiseGrid[idx][1]
	}

	// applies the perlin noise to g.values
	min := float32(math.MaxFloat32)
	for i := 0; i < g.width; i++ {
		for j := 0; j < g.height; j++ {
			x, y := float32(i*(nx-1))/float32(g.width-1), float32(j*(ny-1))/float32(g.height-1)
			rx, ry := int(x), int(y)
			// TODO: why 1-?
			dx, dy := 1-abs(x-float32(rx)), 1-abs(y-float32(ry))
			// Smoothstep is applied, so the second derivative
			// is semi-continuous
			dx, dy = Smoothstep(dx), Smoothstep(dy)
			dots := []float32{}

			// calculate all four dot products
			for a := 0; a < 4; a++ {
				fmt.Print(a%2, a/2, "  ")
				dots = append(dots, dot(rx+(a/2), ry+(a%2), x, y))
			}
			fmt.Println('\n')
			// TODO: delete the 5
			g.values[i][j] = interp(interp(dots[0], dots[1], dy),
				interp(dots[2], dots[3], dy), dx)
			if g.values[i][j] < min {
				min = g.values[i][j]
			}
		}
	}
	// normalize to over 0
	for i := 0; i < g.width; i++ {
		for j := 0; j < g.height; j++ {
			g.values[i][j] += (-min)
		}
	}
	fmt.Println("m", min)
	g.glGrid = g.newGlGrid()
}

func (g *grid) Flatten(points int) {
	if 2*points > g.width || 2*points > g.height {
		panic("twice points bigger than one of the dimensions")
	}
	hanns := make([]float32, points)
	for i := 0; i < points; i++ {
		hanns[i] = float32(HannWindow(float64(i), float64(2*points)))
	}
	for i := 0; i < points; i++ {
		for j := 0; j < g.width; j++ {
			g.values[j][i] *= hanns[i]
			g.values[j][g.height-1-i] *= hanns[i]
		}
	}
	for i := 0; i < points; i++ {
		for j := 0; j < g.height; j++ {
			g.values[i][j] *= hanns[i]
			g.values[g.width-1-i][j] *= hanns[i]
		}
	}
	g.glGrid = g.newGlGrid()
}

// Smoothstep computes the Hermite interpolation
// of the smoothstep function
func Smoothstep(x float32) float32 {
	return x*x*(3-2*x)
}

func (g *grid) newGlGrid(values ...[][]float32) []float32 {
	xpos := g.origin
	glGrid := make([]float32, 0, 6*g.width*g.height)
	i := 0
	for x, arr := range g.values {
		ypos := xpos
		for y, v := range arr {
			valueVector := g.dz
			valueVector.Scale(v)
			if len(values) > 0 {
				valueVector.Scale(values[0][x][y])
			}
			cpos := ypos
			cpos.Add(valueVector)

			// TODO: generalize this shit
			ratio := math.Pi * float32(y) / float32(g.height)
			mult := float32(math.Sin(float64(ratio)))
			// mult := float32(1.0)
			glGrid = append(glGrid, cpos.X, cpos.Y, cpos.Z,
				mult*g.color.X, mult*g.color.Y, mult*g.color.Z)

			ypos.Add(g.dy)
			i += 6
		}
		xpos.Add(g.dx)
	}
	return glGrid
}

func (g *grid) GetGlGrid(wv ...[]float64) []float32 {
	if len(wv) > 0 {
		coefs := make([][]float32, g.width)
		for x := range coefs {
			coefs[x] = make([]float32, g.height)
			for y := range coefs[x] {
				coefs[x][y] = float32(wv[0][y])
			}
		}
		return g.newGlGrid(coefs)
	}

	return g.glGrid
}

type vector2 struct {
	X, Y float32
}

type vector3 struct {
	X, Y, Z float32
}

func (u *vector3) Norm() float32 {
	return float32(math.Sqrt(float64(sqr(u.X) + sqr(u.Y) + sqr(u.Z))))
}

func (u *vector3) Neg() {
	u.X = -u.X
	u.Y = -u.Y
	u.Z = -u.Z
}

func (u *vector3) Add(v vector3) {
	u.X += v.X
	u.Y += v.Y
	u.Z += v.Z
}

func (u *vector3) Scale(c float32) {
	u.X *= c
	u.Y *= c
	u.Z *= c
}

func abs(n float32) float32 {
	if n < 0 {
		return -n
	}
	return n
}

func sqr(n float32) float32 {
	return n * n
}
