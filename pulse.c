#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <pulse/error.h>
#include <pulse/simple.h>

int get_sound(pa_simple *s, uint8_t* buf, int buflen) {
    int error;
    pa_simple_read(s, buf, buflen, &error);
    /* if (pa_simple_read(s, buf, buflen, &error) < 0) { */
    /* fprintf(stderr, __FILE__": pa_simple_read() failed: %s\n", pa_strerror(error)); */
    /*     goto finish; */
    /* } */
    /* And write it to STDOUT */
    return error;
}

pa_simple* new_pa(int *error, char* name) {
    static pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = 44100,
        .channels = 1
    };
    return pa_simple_new(NULL, name, PA_STREAM_RECORD, NULL, "record", &ss, NULL, NULL, error);
}
