package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
	"io/ioutil"
	"strings"
	"fmt"
)

type Shader struct {
	source string
	gln uint32
}

func NewShader(filename string, shaderType uint32) (sh Shader) {
	byteSource, err := ioutil.ReadFile(filename)
	source := string(byteSource)+"\x00"
	if err != nil {
		panic(err)
	}
	shader := gl.CreateShader(shaderType)
	csources, free := gl.Strs(source)

	fmt.Println(byte(source[len(source)-1]))

	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		panic(fmt.Errorf("failed to compile %v: %v", source, log))
	}

	sh = Shader{filename, shader}
	return
}

func (sh *Shader) AttachToProg(prog uint32) {
	gl.AttachShader(prog, sh.gln)
}
