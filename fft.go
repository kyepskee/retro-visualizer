// This is fully ripped off from github.com/takatoh/fft
// Normally I'd just use that, but I needed to deal with it taking up too much memory,
// so the code is copied and slightly tweaked. All credit goes to takatoh
package main

import (
	"math"
	"math/cmplx"
)

// func IFFT(x []complex128, n int, y []complex128) {
// 	for i := 0; i < n; i++ {
// 		// y[i] = complex(real(x[i]), -1.0 * imag(x[i]))
// 		// y[i] = x[i]
// 	}
// 	y = fft(y, n)
// 	for i := 0; i < n; i++ {
// 		y[i] = complex(real(y[i]), -1.0 * imag(y[i]))
// 	}
// }

func FFT(a []float64) []complex128 {
	n := len(a)
	res, out := make([]complex128, n), make([]float64, n)
	_ = out
	ditfft2(a, res, n, 1)
	return res
}

func fft(a []complex128, invert bool) {
	n := len(a)
	if n == 1 {
		return
	}
	a0 := make([]complex128, n/2)
	a1 := make([]complex128, n/2)
	for i := 0; 2*i < n; i++ {
		a0[i] = a[2*i]
		a1[i] = a[2*i+1]
	}

	fft(a0, invert)
	fft(a1, invert)

	ang := 2.0 * math.Pi / float64(n)
	if invert {
		ang = -ang
	}
	w, wn := complex(1, 0), complex(math.Cos(ang), math.Sin(ang))
	for i := 0; 2*i < n; i++ {
		a[i] = a0[i] + w*a1[i]
		a[i+n/2] = a0[i] - w*a1[i]
		if invert {
			a[i] /= 2
			a[i+n/2] /= 2
		}
		w *= wn
	}
}

func ditfft2(x []float64, y []complex128, n, s int) {
	if n == 1 {
		y[0] = complex(x[0], 0)
		return
	}
	ditfft2(x, y, n/2, 2*s)
	ditfft2(x[s:], y[n/2:], n/2, 2*s)
	for k := 0; k < n/2; k++ {
		tf := cmplx.Rect(1, -2*math.Pi*float64(k)/float64(n)) * y[k+n/2]
		y[k], y[k+n/2] = y[k]+tf, y[k]-tf
	}
}

// void fft(vector<cd> & a, bool invert) {
//     int n = a.size();
//     if (n == 1)
//         return;

//     vector<cd> a0(n / 2), a1(n / 2);
//     for (int i = 0; 2 * i < n; i++) {
//         a0[i] = a[2*i];
//         a1[i] = a[2*i+1];
//     }
//     fft(a0, invert);
//     fft(a1, invert);

//     double ang = 2 * PI / n * (invert ? -1 : 1);
//     cd w(1), wn(cos(ang), sin(ang));
//     for (int i = 0; 2 * i < n; i++) {
//         a[i] = a0[i] + w * a1[i];
//         a[i + n/2] = a0[i] - w * a1[i];
//         if (invert) {
//             a[i] /= 2;
//             a[i + n/2] /= 2;
//         }
//         w *= wn;
//     }
// }
