package main

// #cgo pkg-config: libpulse-simple
// #include "pulse.h"
import "C"
import (
	"math"
	"sort"
	"sync"
	"unsafe"
)

var Hann []float64

func init() {
	Hann = make([]float64, 8)
	for i := 0.0; i <= 7.0; i++ {
		Hann[int(i)] = HannWindow(i, 7)
	}
}

func Record(wv *Waveform, samples, resLen, bufSize uint32) {
	var error C.int
	error = 0

	s := C.new_pa(&error, C.CString("RetroVis"))
	// TODO: handle error
	buf := make([]int16, bufSize*samples)
	converted := make([]float64, bufSize*samples)
	// converted := make([]complex128, 8*samples)
	res := make([]float64, resLen)
	cres := make([]complex128, resLen)

	defer C.pa_simple_free(s)
	for {
		copy(buf[:(bufSize-1)*samples], buf[samples:])
		C.pa_simple_read(s, unsafe.Pointer(&buf[0]), C.ulong(2*samples), &error)

		for i, v := range buf {
			converted[i] = float64(v)
		}
		cres = FFT(converted)[:resLen]
		for i, v := range cres {
			res[i] = math.Sqrt(real(v)*real(v) + imag(v)*imag(v))
		}
		wv.ApplyNewAudio(res)
	}
}

type Waveform struct {
	wave []float64
	wavePadded []float64
	min  float64
	mux  sync.Mutex
}

func NewWaveform(samples, padding int) (wave *Waveform) {
	padded := make([]float64, samples+padding)
	simple := padded[padding:]
	wave = &Waveform{simple, padded, 0.0, sync.Mutex{}}
	return
}

func (wv *Waveform) ApplyNewAudio(audio []float64) {
	wv.mux.Lock()

	// previous state for Smooth() to compare to
	// TODO: figure out where to HannAndLog to
	prev := make([]float64, len(wv.wavePadded))
	copy(prev, wv.wavePadded)
	HannAndLog(wv.wave, audio)

	wv.Smooth(wv.wavePadded, prev)
	wv.mux.Unlock()
}

// NOTE: does *NOT* lock mutex
func (wv *Waveform) Smooth(new, prev []float64) {
	sorted := make([]float64, len(new))
	copy(sorted, new)
	sort.Float64s(sorted)

	prevMin := wv.min

	percentile := .5
	limit := sorted[int((1-percentile)*float64(len(sorted)))]
	for i, v := range new {
		new[i] -= limit
		if v <= limit {
			new[i] = 0.0
		}
	}
	for i, _ := range wv.wave {
		alpha, alphaMin := .05, .1
		if prev[i] < new[i] {
			alpha = .5
		}
		new[i] = alpha*(new[i]-prev[i]) + prev[i] ///float64(samplesPerSmooth)
		wv.min = alphaMin*wv.min + (1-alphaMin)*prevMin
	}
}

func HannAndLog(dst, src []float64, padding ...int) {
	inputsPerPoint := float64(len(src)) / float64(len(dst))
	inputsPerSmooth := int(5 * inputsPerPoint)
	before := make([]float64, len(dst))
	copy(before, dst)
	for i, _ := range dst {
		start := int(math.Floor(inputsPerPoint * float64(i)))
		dst[i] = 0
		for j := 0; j <= inputsPerSmooth && start+j < len(src); j++ {
			c := src[start+j]
			dst[i] += HannWindow(float64(j), float64(inputsPerSmooth)) * c
		}
		if dst[i] > 1.0 {
			dst[i] = math.Log(dst[i])
		}
	}
}

func HannWindow(n, N float64) float64 {
	return math.Pow(math.Sin(math.Pi*n/N), 2)
}
