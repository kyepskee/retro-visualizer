#include <stdlib.h>
#include <pulse/simple.h>

#ifndef PULSE_H_
#define PULSE_H_

pa_simple* new_pa(int *error, char* name);
int get_sound(pa_simple *s, uint8_t* buf, int buflen);

#endif
