package main

import (
	"flag"
	"fmt"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/go-gl/mathgl/mgl32"
	"runtime"
	"os"
	"runtime/pprof"
	"log"
	"time"
)

var (
	triangle = []float32{
		0, .5, 0,
		-.5, -.5, 0,
		.5, -.5, 0,
	}
	square = []float32{
		.5, .5, 0, 0.5, 0, 1,
		.5, -.5, 0, 0.5, 0, 0,
		-.5, -.5, 0, 0, 0, 1,
		-.5, .5, 0, 0.5, 0, 0,
	}
	squareIndices = []int32{
		0, 1, 2, 3,
	}
	width, height = 1600, 1000
)

type Vao struct {
	vbo, ebo, vao uint32
}

var noiseLU []float64

var vaos = make(map[uint32]Vao)
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	runtime.LockOSThread()

	win := initGLFW()
	defer glfw.Terminate()

	prog := initOpenGL()

	// vao := makeEVao(square, squareIndices)
	// _ = vao

	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	var mva int32
	gl.GetIntegerv(gl.MAX_VERTEX_ATTRIBS, &mva)

	modelLoc := gl.GetUniformLocation(prog, gl.Str("model\x00"))
	cameraLoc := gl.GetUniformLocation(prog, gl.Str("camera\x00"))
	projectionLoc := gl.GetUniformLocation(prog, gl.Str("projection\x00"))

	modelMat := mgl32.Ident4()
	cameraMat := mgl32.LookAt(0, 3.5, -5, 0, 3, 0, 0, 1, 0)
	// cameraMat := mgl32.Ident4()
	projectionMat := mgl32.Perspective(mgl32.DegToRad(45), float32(width)/float32(height), 0.1, 50)
	// projectionMat := mgl32.Ident4()

	gl.UseProgram(prog)

	gl.UniformMatrix4fv(cameraLoc, 1, false, &cameraMat[0])
	gl.UniformMatrix4fv(projectionLoc, 1, false, &projectionMat[0])
	gl.UniformMatrix4fv(modelLoc, 1, false, &modelMat[0])

	gridWidth, gridHeight, gridSize := int32(50), int32(50), float32(.2)
	dx, dy, dz := vector3{-1, 0, 0}, vector3{0, 0, 1}, vector3{0, 1, 0}
	dx.Scale(gridSize)
	dy.Scale(gridSize)
	dz.Scale(gridSize)

	grid := NewGrid(int(gridWidth), int(gridHeight),
		vector3{4, 0, 5},
		dx, dy, dz,
		vector3{0.5, 0, 1})
	grid.Perlin()
	grid.Flatten(3)

	var gridIndices []int32
	gridIndices = make([]int32, 0, 8*gridHeight*gridWidth)

	for x := int32(0); x < gridWidth-1; x++ {
		for y := int32(0); y < gridHeight-1; y++ {
			sx, bx := x, x+1
			sy, by := y*gridWidth, (y+1)*gridWidth

			gridIndices = append(gridIndices, sx+sy, sx+by,
				sx+by, bx+by,
				bx+by, bx+sy,
				bx+sy, sx+sy)
		}
	}

	glGrid := grid.GetGlGrid()

	gridEvao, vao := makeEVao(glGrid, gridIndices)

	var ctime, lastTime int64
	lastTime = time.Now().UnixNano()
	t := 0.0

	wave := NewWaveform(200, 0)
	go Record(wave, 1024, 400, 8)
	for len(wave.wave) == 0 {}
	fmt.Printf("CPU: %d\n", runtime.NumCPU())

	for !win.ShouldClose() {
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		ctime = time.Now().UnixNano()
		t += float64((ctime - lastTime) / 10000000)
		lastTime = ctime

		gl.UseProgram(prog)
		wave.mux.Lock()
		// TODO: maybe re-add this and make Smooth() work
		// wave.Smooth()

		glGrid = grid.GetGlGrid(wave.wavePadded)
		// glGrid = grid.GetGlGrid()

		ctime = time.Now().UnixNano()
		t += float64((ctime - lastTime) / 10000000)
		// fmt.Printf("\r%f FPS", 1000000000/(float64(ctime-lastTime)))
		lastTime = ctime
		wave.mux.Unlock()
		vao.Delete()
		gridEvao, vao = makeEVao(glGrid, gridIndices)
		gl.UniformMatrix4fv(modelLoc, 1, false, &modelMat[0])

		gl.BindVertexArray(gridEvao)
		gl.DrawElements(gl.LINES, int32(len(gridIndices)), gl.UNSIGNED_INT, nil)
		gl.BindVertexArray(0)

		glfw.PollEvents()
		win.SwapBuffers()
	}
}

// func init() {
// 	noiseLU = make([]float64, )
// 	for i, _ := noiseLU {

// 	}
// }

func initGLFW() *glfw.Window {
	if err := glfw.Init(); err != nil {
		panic(err)
	}

	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 3)
	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	win, err := glfw.CreateWindow(width, height, "GoGl", nil, nil)
	if err != nil {
		panic(err)
	}
	win.MakeContextCurrent()
	return win
}

func initOpenGL() uint32 {
	if err := gl.Init(); err != nil {
		panic(err)
	}

	prog := gl.CreateProgram()

	vertexShader := NewShader("./vertex.glsl", gl.VERTEX_SHADER)
	vertexShader.AttachToProg(prog)

	fragmentShader := NewShader("./frag.glsl", gl.FRAGMENT_SHADER)
	fragmentShader.AttachToProg(prog)

	gl.LinkProgram(prog)
	return prog
}

func makeVertexArray(points []float32) uint32 {
	var vbo, vao uint32
	gl.GenBuffers(1, &vbo)
	gl.GenVertexArrays(1, &vao)

	gl.BindVertexArray(vao)

	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(points), gl.Ptr(points), gl.STATIC_DRAW)

	gl.EnableVertexAttribArray(0)
	gl.BindBuffer(gl.ARRAY_BUFFER, vao)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 3*4, nil)

	return vao
}

func makeEVao(points []float32, indices []int32) (uint32, Vao) {
	var vbo, ebo, vao uint32
	gl.GenBuffers(1, &vbo)
	gl.GenBuffers(1, &ebo)
	gl.GenVertexArrays(1, &vao)
	vaoo := Vao{vbo, ebo, vao}

	gl.BindVertexArray(vao)

	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(points), gl.Ptr(&points[0]), gl.DYNAMIC_DRAW)

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, 4*len(indices), gl.Ptr(&indices[0]), gl.DYNAMIC_DRAW)

	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 6*4, gl.PtrOffset(0))

	gl.EnableVertexAttribArray(1)
	gl.VertexAttribPointer(1, 3, gl.FLOAT, false, 6*4, gl.PtrOffset(3*4))

	return vao, vaoo
}

func (v Vao) Delete() {
	gl.DeleteBuffers(1, &v.vbo)
	gl.DeleteBuffers(1, &v.ebo)
	gl.DeleteVertexArrays(1, &v.vao)
}

func max(a, b float32) float32 {
	if a > b {
		return a
	}
	return b
}

func noise(x, y int64) {

}
